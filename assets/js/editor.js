var inscrybmde = new InscrybMDE({
  autoDownloadFontAwesome: true,
  status:  true,
  status: ["autosave", "lines", "words", "cursor", {
    className: "keystrokes",
    defaultValue: function(el) {
      this.keystrokes = 0;
      el.innerHTML = "0 Keystrokes";
    },
    onUpdate: function(el) {
      el.innerHTML = ++this.keystrokes + " Keystrokes";
    }
  }],
  forceSync: true,
  minHeight: "200px",
  indentWithTabs: true,
  autoRender: true,
  spellChecker: false,
  element: document.getElementById('textarea1'),
  toolbar: ["bold", "italic","strikethrough", "heading", "|", "code", "quote", "unordered-list", "ordered-list", "clean-block", "|", "link",  "image", "horizontal-rule",
  {
    name: "YouTube",
    action: embedYoutubeVideo,
    className: "fa fa-youtube-play", // Look for a suitable icon
    title: "YouTube (Ctrl/Cmd-Alt-R)",
  }, {
    name: "Twitch",
    action: embedTwitch,
    className: "fa fa-twitch", // Look for a suitable icon
    title: "Twitch (Ctrl/Cmd-Alt-R)",
  },
  {
    name: "Emoji",
    action: emojis,
    className: "fa fa-twitch", // Look for a suitable icon
    title: "Emoji (Ctrl/Cmd-Alt-R)",
  },
  "|", "preview", "side-by-side", "fullscreen", "|",
]
});
function embedYoutubeVideo(editor) {

  var cm = editor.codemirror;
  var output = '';
  var selectedText = cm.getSelection();
  var text = selectedText || 'Video Link';

  output = '{% youtube "' + text + '" %}';
  cm.replaceSelection(output);

}
function emojis(editor){
  chargeEmojis();
  console.log(editor);
    $(editor.codemirror.getTextArea()).emojiPicker('toggle');
    // keyup event is fired
    $(editor.codemirror.getTextArea()).on("keyup", function () {
      console.log($(this));
      editor.codemirror.replaceSelection($(this).val());
      deleteTheFuckingEMOJIS();
    });
}

function chargeEmojis() {
  $(inscrybmde.codemirror.getTextArea()).emojiPicker({
    width: '300px',
    height: '200px',
    button: true
  });
}
     function deleteTheFuckingEMOJIS(){
      $(inscrybmde.codemirror.getTextArea()).emojiPicker('destroy');
}
function embedTwitch(editor) {

  var cm = editor.codemirror;
  var output = '';
  var selectedText = cm.getSelection();
  var text = selectedText || 'Twitch Link';

  output = '{% twitch "' + text + '" %}';
  cm.replaceSelection(output);

}