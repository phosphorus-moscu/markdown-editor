$(document).ready(function(){
  $('.datepicker').datepicker({
              firstDay: true,
              format: 'yyyy-mm-dd',
              i18n: {
                  months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                  monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
                  weekdays: ["Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                  weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                  weekdaysShort: ["Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                  weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"],
                  today: 'Hoy',
                  clear: 'Limpiar',
                  close: 'Ok',
                  cancel: 'Cancelar',
                  nextMonth: 'Siguiente mes',
                  previousMonth: 'Mes anterior',
                  labelMonthSelect: 'Selecciona un mes',
                  labelYearSelect: 'Selecciona un año'
              },
          });
});
